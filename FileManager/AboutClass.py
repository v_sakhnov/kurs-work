from PyQt5 import QtWidgets
from multiprocessing import Process, Queue
from FileManager import About
from multiprocessing.connection import Client

# text = """Файловый менеджер:
# 1. операционная система - Microsoft Windows;
# 2. способ межпроцессного взаимодействия - Файл.
# """


class AboutClass(QtWidgets.QMainWindow, About.Ui_About):
    def __init__(self, file_path):
        super().__init__()
        self.setupUi(self)
        self.file_path = file_path
        # self.textEdit.setText(text)
        self.data = ''

    def read_file(self):
        file = open(self.file_path, 'r')
        self.data = file.readlines()
        result_string =''
        for string in self.data:
            result_string += string
        # print(self.data))
        self.textEdit.setText(result_string)
        file.close()

    # срабатывает при нажатии на крестик(тоже переопределён)
    def closeEvent(self, event):
        event.accept()

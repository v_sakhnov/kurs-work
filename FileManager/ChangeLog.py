# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ChangeLog.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ChangeLog(object):
    def setupUi(self, ChangeLog):
        ChangeLog.setObjectName("ChangeLog")
        ChangeLog.resize(192, 133)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ChangeLog.sizePolicy().hasHeightForWidth())
        ChangeLog.setSizePolicy(sizePolicy)
        ChangeLog.setMinimumSize(QtCore.QSize(192, 133))
        ChangeLog.setMaximumSize(QtCore.QSize(192, 133))
        self.centralwidget = QtWidgets.QWidget(ChangeLog)
        self.centralwidget.setObjectName("centralwidget")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(20, 40, 161, 20))
        self.lineEdit.setObjectName("lineEdit")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 10, 61, 20))
        self.label.setObjectName("label")
        self.okButton = QtWidgets.QPushButton(self.centralwidget)
        self.okButton.setGeometry(QtCore.QRect(20, 80, 75, 23))
        self.okButton.setObjectName("okButton")
        self.cancelButton = QtWidgets.QPushButton(self.centralwidget)
        self.cancelButton.setGeometry(QtCore.QRect(100, 80, 75, 23))
        self.cancelButton.setObjectName("cancelButton")
        ChangeLog.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(ChangeLog)
        self.statusbar.setObjectName("statusbar")
        ChangeLog.setStatusBar(self.statusbar)

        self.retranslateUi(ChangeLog)
        QtCore.QMetaObject.connectSlotsByName(ChangeLog)

    def retranslateUi(self, ChangeLog):
        _translate = QtCore.QCoreApplication.translate
        ChangeLog.setWindowTitle(_translate("ChangeLog", "Сменить Имя"))
        self.label.setText(_translate("ChangeLog", "Новое Имя"))
        self.okButton.setText(_translate("ChangeLog", "OK"))
        self.cancelButton.setText(_translate("ChangeLog", "Отмена"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ChangeLog = QtWidgets.QMainWindow()
    ui = Ui_ChangeLog()
    ui.setupUi(ChangeLog)
    ChangeLog.show()
    sys.exit(app.exec_())


from PyQt5 import QtWidgets
from FileManager import mask


class MaskClass(QtWidgets.QMainWindow, mask.Ui_MaskWindow):
    def __init__(self, file_path):
        super().__init__()
        self.setupUi(self)
        self.file_path = file_path
        self.result_array = []

    def read_file(self):
        print('reading...')
        file = open(self.file_path, 'r')
        # while file.:
        #     self.result_array.append(file.readline)
        self.result_array = file.readlines()
        self.print_data()
        file.close()

    def print_data(self):
        self.listWidget.clear()
        for string in self.result_array:
            self.listWidget.addItem(string)

    def closeEvent(self, event):
        event.accept()

from PyQt5 import QtWidgets
from FileManager import ChangeLog


class ChangeLogClass(QtWidgets.QMainWindow, ChangeLog.Ui_ChangeLog):
    def __init__(self, file_path):
        super().__init__()
        self.setupUi(self)
        self.file_path = file_path
        self.okButton.clicked.connect(self.write_file)
        self.cancelButton.clicked.connect(self.close)

    # записывает новое имя в файл
    def write_file(self):
        data = self.lineEdit.text()
        file = open(self.file_path, 'w')
        file.write(data)
        file.flush()
        file.close()
        self.close()

    def closeEvent(self, event):
        event.accept()
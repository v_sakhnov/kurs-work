from PyQt5 import QtWidgets
from FileManager import Modules


class ModulesClass(QtWidgets.QMainWindow, Modules.Ui_ModulesWindow):

    def __init__(self, file_path):
        super().__init__()
        self.setupUi(self)
        self.file_path = file_path
        self.data = []

    # Читает из файла
    def read_file(self):
        file = open(self.file_path, 'r')
        self.data = file.readlines()
        file.close()
        self.print_data()

    # выводит считанные данные в listWidget
    def print_data(self):
        self.listWidget.clear()
        for string in self.data:
            self.listWidget.addItem(string)

    def closeEvent(self, event):
        event.accept()

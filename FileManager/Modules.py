# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Modules.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ModulesWindow(object):
    def setupUi(self, ModulesWindow):
        ModulesWindow.setObjectName("ModulesWindow")
        ModulesWindow.resize(349, 600)
        self.centralwidget = QtWidgets.QWidget(ModulesWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(20, 30, 311, 531))
        self.listWidget.setObjectName("listWidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(120, 10, 121, 20))
        self.label.setObjectName("label")
        ModulesWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(ModulesWindow)
        self.statusbar.setObjectName("statusbar")
        ModulesWindow.setStatusBar(self.statusbar)

        self.retranslateUi(ModulesWindow)
        QtCore.QMetaObject.connectSlotsByName(ModulesWindow)

    def retranslateUi(self, ModulesWindow):
        _translate = QtCore.QCoreApplication.translate
        ModulesWindow.setWindowTitle(_translate("ModulesWindow", "MainWindow"))
        self.label.setText(_translate("ModulesWindow", "Количество модулей"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ModulesWindow = QtWidgets.QMainWindow()
    ui = Ui_ModulesWindow()
    ui.setupUi(ModulesWindow)
    ModulesWindow.show()
    sys.exit(app.exec_())


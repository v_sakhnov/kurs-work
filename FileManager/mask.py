# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mask.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MaskWindow(object):
    def setupUi(self, MaskWindow):
        MaskWindow.setObjectName("MaskWindow")
        MaskWindow.resize(434, 600)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MaskWindow.sizePolicy().hasHeightForWidth())
        MaskWindow.setSizePolicy(sizePolicy)
        MaskWindow.setMinimumSize(QtCore.QSize(434, 600))
        MaskWindow.setMaximumSize(QtCore.QSize(434, 600))
        self.centralwidget = QtWidgets.QWidget(MaskWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(10, 40, 411, 531))
        self.listWidget.setObjectName("listWidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(100, 10, 241, 20))
        self.label.setObjectName("label")
        MaskWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MaskWindow)
        self.statusbar.setObjectName("statusbar")
        MaskWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MaskWindow)
        QtCore.QMetaObject.connectSlotsByName(MaskWindow)

    def retranslateUi(self, MaskWindow):
        _translate = QtCore.QCoreApplication.translate
        MaskWindow.setWindowTitle(_translate("MaskWindow", " Маски процессов"))
        self.label.setText(_translate("MaskWindow", "Маски привязки процессов к процессорам"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MaskWindow = QtWidgets.QMainWindow()
    ui = Ui_MaskWindow()
    ui.setupUi(MaskWindow)
    MaskWindow.show()
    sys.exit(app.exec_())


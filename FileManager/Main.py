import threading

import pythoncom
import win32api
import win32con
import win32process
import wmi
from PyQt5 import QtCore, QtWidgets
import sys, os, shutil, pathlib, psutil
from multiprocessing import Process
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import qApp

from FileManager import KursWork
from FileManager.AboutClass import AboutClass
from multiprocessing.connection import Listener, Client

from FileManager.ChangeLogClass import ChangeLogClass
from FileManager.MaskClass import MaskClass
from FileManager.ModulesClass import ModulesClass

log_dir = os.getcwd() + os.sep + 'log'
log_file_name = 'log.txt'
if not os.path.exists(log_dir):
    os.mkdir(log_dir)

exchanging_dir = os.getcwd() + os.sep + 'exchanging'
if not os.path.exists(exchanging_dir):
    os.mkdir(exchanging_dir)

run = True

if os.path.exists(log_dir) and len(os.listdir(log_dir)) != 0:
    log_file_name = os.listdir(log_dir)[0]

file = open(log_dir + os.sep + log_file_name, 'w')

processes = []


# мониторит процессы и пишет логи
def get_process():
    pythoncom.CoInitialize()
    c = wmi.WMI()
    process_watcher = c.Win32_Process.watch_for("creation")
    while run:
        try:
            new_process = process_watcher()
            file.write(str(new_process.Name) + " " + new_process.CreationDate + '\n')
            file.flush()
        except ValueError:
            print("Error")
    file.close()


thread = threading.Thread(target=get_process)
thread.start()

new_log = ""


class ExampleApp(QtWidgets.QMainWindow, KursWork.Ui_MainWindow):

    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)

        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath(QtCore.QDir.currentPath())

        self.treeView.setModel(self.model)
        self.treeView.setRootIndex(self.model.index("D:\\PGUTI\\Python_Repositories\\KW\\root"))
        self.treeView.installEventFilter(self)

        self.processes = psutil.process_iter()
        #self.listWidget.installEventFilter(self)

        path = self.treeView.windowFilePath()
        self.current_path = ""
        self.model.setReadOnly(False)
        self.buffer_string = ""
        self.about = None
        self.page_errors = 0
        self.modulesClass = None
        self.maskClass = None
        self.ChangeLog = None
        # menu
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('Файл')
        open_button = QtWidgets.QAction('Открыть', fileMenu)
        fileMenu.addAction(open_button)
        close_button = QtWidgets.QAction('Закрыть', fileMenu)
        close_button.triggered.connect(qApp.quit)
        fileMenu.addAction(close_button)

        editMenu = mainMenu.addMenu('Изменить')
        change_log_name_button = QtWidgets.QAction('Изменить имя лог-файла', editMenu)
        change_log_name_button.triggered.connect(self.show_change_log_name)
        editMenu.addAction(change_log_name_button)

        toolsMenu = mainMenu.addMenu('Инструменты')
        task_manager_button = QtWidgets.QAction('Диспетчер задач', toolsMenu)
        task_manager_button.triggered.connect(self.open_task_manager)
        toolsMenu.addAction(task_manager_button)
        calculator_button = QtWidgets.QAction('Калькулятор', toolsMenu)
        calculator_button.triggered.connect(self.open_calculator)
        toolsMenu.addAction(calculator_button)
        paint_button = QtWidgets.QAction('Paint', toolsMenu)
        paint_button.triggered.connect(self.open_paint)
        toolsMenu.addAction(paint_button)

        viewMenu = mainMenu.addMenu('View')
        processes_button = QtWidgets.QAction('Используемые модули', viewMenu)
        processes_button.triggered.connect(self.modules)
        viewMenu.addAction(processes_button)
        pageError_button = QtWidgets.QAction('Макска процесса', viewMenu)
        pageError_button.triggered.connect(self.mask)
        viewMenu.addAction(pageError_button)

        helpMenu = mainMenu.addMenu('Помощь')
        about_button = QtWidgets.QAction('О программе', helpMenu)
        about_button.triggered.connect(self.write_about)
        helpMenu.addAction(about_button)

        self.mask_path = exchanging_dir + os.sep + 'mask.txt'
        self.about_path = exchanging_dir + os.sep + 'about.txt'
        self.change_log_path = exchanging_dir + os.sep + 'change_log.txt'
        self.module_path = exchanging_dir + os.sep + 'module.txt'

        # endMenu

    # находит модули, используемые процессами и записывает в файл
    def modules(self):
        module_file = open(self.module_path, 'w')
        for process in self.processes:
            try:
                if process.pid:
                    ph = win32api.OpenProcess(win32con.MAXIMUM_ALLOWED, False, process.pid)
                    count = win32process.EnumProcessModules(ph)
                    print(len(count))
                    module_file.write(
                        'Имя: ' + str(process.name()) + ' Количество модулей, используемых процессом:' + str(
                            len(count)) + '\n')
                    module_file.flush()

            except:
                print('error')
        module_file.close()
        self.show_modules() # вызывает окошко

    # создаёт экземпляр окна с модулями и показывает его
    def show_modules(self):
        if not self.modulesClass:
            self.modulesClass = ModulesClass(self.module_path)
        self.modulesClass.read_file()
        self.modulesClass.show()

    # создаёт экземпляр окна с инфой о программе и показывает его
    def open_about(self):
        # self.write_about()
        if not self.about:
            self.about = AboutClass(self.about_path)
        self.about.read_file()
        self.about.show()

    # записывает в файл этот текст
    def write_about(self):
        file = open(self.about_path, 'w')
        text = """Файловый менеджер:
        1. операционная система - Microsoft Windows;
        2. способ межпроцессного взаимодействия - Файл;
        3. дополнительное задание №1 - маска привязки процесса к процессорам; 
        4. дополнительное задание №2 - количество модулей, используемых процессом.
        """
        file.write(text)
        file.flush()
        file.close()
        self.open_about()

    # создаёт экземпляр окна со сменой имени лога и показывает его
    def show_change_log_name(self):
        if not self.ChangeLog:
            self.ChangeLog = ChangeLogClass(self.change_log_path)
        self.ChangeLog.show()

    # читает из файла новое имя для лога
    def read_file_log(self):
        file_log = open(self.change_log_path, 'r')
        global new_log
        new_log = file_log.readline()
        file_log.close()

    # создаёт экземпляр окна с масками и показывает его
    def show_mask(self):
        if not self.maskClass:
            self.maskClass = MaskClass(self.mask_path)
        self.maskClass.read_file()
        self.maskClass.show()

    # записывает в файл имя процесса и используемые им процессы
    def mask(self):
        print('pasd')
        mask_file = open(self.mask_path, 'w')
        for process in self.processes:
            print('processing')
            name = str(process.as_dict(attrs=['name'])).split(':')
            name = name[1]
            name = name.split('\'')
            cpu_mask = str(process.as_dict(attrs=['cpu_affinity'])).split(':')
            cpu_mask = cpu_mask[1]
            cpu_mask = cpu_mask.split('}')
            if not cpu_mask[0].__contains__('None'):
                mask_file.write('Имя процесса: ' + name[1] + ', ' + 'Используемые процессоры: ' + cpu_mask[0] + '\n')
                mask_file.flush()
        mask_file.close()
        self.show_mask()

    # запуск утилит
    def open_task_manager(self):
        os.system(r'C:\Windows\system32\taskmgr.exe')

    def open_calculator(self):
        os.system(r'C:\Windows\system32\calc.exe')

    def open_paint(self):
        os.system(r'C:\Windows\system32\mspaint.exe')

    # создает папку
    def create_dir(self):
        index = self.treeView.currentIndex()
        path = self.model.filePath(index)
        path_index = 0

        new_path = path + '\\new_folder'

        my_file = pathlib.Path(new_path)

        while my_file.exists():
            path_index += 1
            new_path = path + '\\new_folder' + str(path_index)
            my_file = pathlib.Path(new_path)

        os.mkdir(new_path)
        return

    # создает файл
    def create_file(self):
        index = self.treeView.currentIndex()
        path = self.model.filePath(index)
        path_index = 0

        new_path = path + '/new_file'

        my_file = pathlib.Path(new_path)

        while my_file.exists():
            path_index += 1
            new_path = path + '/new_file' + str(path_index)
            my_file = pathlib.Path(new_path)

        open(new_path, 'w').close()

    # копирует
    def copy_script(self):
        index = self.treeView.currentIndex()
        path = self.model.filePath(index)

        self.buffer_string = path
        return

    # вставляет
    def paste_script(self):
        index = self.treeView.currentIndex()
        path = self.model.filePath(index)

        shutil.copy(self.buffer_string, path)
        return

    # удаляет
    def delete_script(self):
        index = self.treeView.currentIndex()
        path = self.model.filePath(index)

        if os.path.isdir(path):
            shutil.rmtree(path)
        elif not os.path.isdir(path):
            os.remove(path)

        return

    # переходит в выбранную папку
    def open_script(self):
        index = self.treeView.currentIndex()
        path = self.model.filePath(index)

        self.current_path = path

        self.treeView.setRootIndex(self.model.index(path))

    # возвращается на папку назад
    def back_script(self):
        new_paths = self.current_path.split('/')

        print(self.current_path)

        new_path = ""

        for i in range(len(new_paths) - 1):
            new_path += new_paths[i] + '/'

        new_path = new_path[:-1]
        self.current_path = new_path

        self.treeView.setRootIndex(self.model.index(new_path))

    # создается контекстное меню(метод переопределённый)
    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.ContextMenu and obj is self.treeView:
            menu = QtWidgets.QMenu()
            change_root_path = QtWidgets.QAction('Открыть', menu)
            menu.addAction(change_root_path)
            create_folder = QtWidgets.QAction('Создать', menu)
            menu.addAction(create_folder)
            create_file = QtWidgets.QAction('Создать файл', menu)
            menu.addAction(create_file)
            copy_folder = QtWidgets.QAction('Копировать', menu)
            menu.addAction(copy_folder)
            paste_folder = QtWidgets.QAction('Вставить', menu)
            menu.addAction(paste_folder)
            delete_folder = QtWidgets.QAction('Удалить', menu)
            menu.addAction(delete_folder)
            back_path = QtWidgets.QAction('Назад', menu)
            menu.addAction(back_path)

            action = menu.exec_(event.globalPos())

            if action == create_folder:
                self.create_dir()
            elif action == copy_folder:
                self.copy_script()
            elif action == paste_folder:
                self.paste_script()
            elif action == delete_folder:
                self.delete_script()
            elif action == change_root_path:
                self.open_script()
            elif action == back_path:
                self.back_script()
            elif action == create_file:
                self.create_file()

        return super(ExampleApp, self).eventFilter(obj, event)


# запуск всего говна
def main():
    app = QtWidgets.QApplication(sys.argv)
    window = ExampleApp()
    window.show()
    app.exec()
    window.read_file_log()
    thread.join(0.1)
    file.close()
    if new_log != '':
        os.rename(log_dir + os.sep + os.listdir(log_dir)[0], log_dir + os.sep + new_log + '.txt')


if __name__ == '__main__':
    main()
